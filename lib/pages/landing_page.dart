import 'package:flutter/material.dart';

import '../ejercicios/ejercicio_1/hola_mundo.dart';
import '../ejercicios/ejercicio_2/startup_namer.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ejercicios de Flutter'),
      ),
      body: ListaEjemplos(),
    );
  }
}

class ListaEjemplos extends StatefulWidget {
  
  @override
  State createState() => new ListaEjemplosState();
}

class ListaEjemplosState extends State<ListaEjemplos> {
  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: [
        _construyeItem('Hola Mundo', 'Mi Primer Ejercicio en Flutter', HolaMundo()),
        _construyeItem('Nombres para StartUps', 'Seleccionando Nombres para una Startup', StartupNamer()),
      ],
    );
  }

  Widget _construyeItem(String textTitle, String textSubTitle, ejercicio) {
    return new ListTile(
      title: new Text(textTitle),
      subtitle: new Text(textSubTitle),
      leading: new Icon(Icons.map),
      onTap: () {
        //print(textTitle);
        _verEjercicio(textTitle, ejercicio);
      },
    );
  }

  void _verEjercicio(String textTitle, ejercicio) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return ejercicio;
        },
      ),
    );
  }
}