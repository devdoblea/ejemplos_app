Primeros Ejemplos hechos en Flutter
Para iniciar, he realizado 2 ejercicios de flutter (los primeros "Hola mundo")
pero con la intención de ir agregando mas a medida que voy avanzado.

![Pantalla Inicial](assets/images/Screenshot_2019-11-24-15-27-48.png)

# ejemplos_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
